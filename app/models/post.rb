class Post < ApplicationRecord
  # Validando que el título y contenido del post estén presentes al momento de crear un nuevo Post
  validates :title, presence: true
  validates :content, presence: true
end
