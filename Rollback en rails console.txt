2.4.1 :004 > @post = Post.new()
 => #<Post id: nil, title: nil, content: nil, created_at: nil, updated_at: nil> 
2.4.1 :005 > @post.save
   (0.1ms)  begin transaction
   (0.1ms)  rollback transaction
 => false 
2.4.1 :006 > @post.errors
 => #<ActiveModel::Errors:0x00000004e3fe50 @base=#<Post id: nil, title: nil, content: nil, created_at: nil, updated_at: nil>, @messages={:title=>["can't be blank"]}, @details={:title=>[{:error=>:blank}]}> 
2.4.1 :007 > @post = Post.new(title: 'Título :)', content: 'contenidooo')
 => #<Post id: nil, title: "Título :)", content: "contenidooo", created_at: nil, updated_at: nil> 
2.4.1 :008 > @post.save
   (0.1ms)  begin transaction
  SQL (0.4ms)  INSERT INTO "posts" ("title", "content", "created_at", "updated_at") VALUES (?, ?, ?, ?)  [["title", "Título :)"], ["content", "contenidooo"], ["created_at", "2018-05-17 02:35:10.396735"], ["updated_at", "2018-05-17 02:35:10.396735"]]
   (69.7ms)  commit transaction
 => true 
